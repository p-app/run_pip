## Run [pip](https://pypi.org/project/pip/) project.

This project is intented to run pip command throught it\`s programming API, 
but not throught `subprocess` python core module or any other process modules.

### Usage example.
```python
from run_pip import run_pip

def install_wheel_package():
    """It installs wheel package to interpreter environment."""
    run_pip('install', 'wheel')
    
install_wheel_package()  # installation occurs.
```


### Available helper functions.
 - `run_pip(*args)` - run any pip command (depends on `args`).
 - `run_pip_install(*args)` - run pip "install" command (with `args`).
 - `run_pip_uninstall(*args)` - run pip "uninstall" command (with `args`).
 
 
### Pip compatibility.
Package has been tested with 9.x, 10.x and 18.x versions of 
[pip](https://pypi.org/project/pip/) package.
