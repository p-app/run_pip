from setuptools import find_packages, setup

setup(
    name='run_pip',
    version='0.1.0',
    classifiers=[
        "Intended Audience :: Developers",
        "Programming Language :: Python",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
    ],
    packages=find_packages(),
    url='https://bitbucket.org/p-app/run_pip',
    license='MIT',
    author='Vladimir V. Pivovarov',
    author_email='admin@p-app.ru',
    description='Programming API for calling pip commands with various pip '
                'versions.',
    install_requires=['pip']
)
