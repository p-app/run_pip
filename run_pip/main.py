from functools import partial
from os import environ

try:
    from pip import main
except ImportError:
    # noinspection PyProtectedMember
    from pip._internal import main


__all__ = [
    'run_pip',
    'run_pip_install',
    'run_pip_uninstall',
]


def run_pip(*args):
    """Manual programming call of pip."""
    main(list(args))

    # Deleting ENV variable for correct work of pip>=18.0
    environ.pop('PIP_REQ_TRACKER', None)


# Pip install shortcut.
run_pip_install = partial(run_pip, 'install')

# Pip uninstall shortcut.
run_pip_uninstall = partial(run_pip, 'uninstall')
